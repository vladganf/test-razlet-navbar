"use strict"
document.addEventListener("DOMContentLoaded", function() {

	// console.log(document.getElementsByClassName('.menu').getElementsByClassName('.item'));

	// document.querySelectorAll('.menu .item').addEventListener('click', function() {
	// 	document.querySelectorAll('.level-2').classList.toggle('active');
	// });

	[].forEach.call(document.querySelectorAll('.menu .item'), function(item) {
		item.addEventListener('click', function() {
			console.log(item);
			item.querySelector('.level-2').classList.toggle('active');
			// document.querySelector('.level-2').classList.toggle('active');
		});
	});

});
